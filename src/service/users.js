const mFlixDb = require('../model/users');

const mFlixService = {};

/* Invoke this method to throw error with appropriate message and status code */
const throwError = (errMessage, errStatus) => {
    let err = new Error(errMessage);
    err.status = errStatus;
    throw err;
}

mFlixService.showMovies = async () => {
    let movieData = await mFlixDb.showMovies();
    console.log(`movieData: ${movieData}`);

    if(movieData.length <= 0){
        throwError('We have halted our services for upgrade. Check back soon', 404);
    }

    return movieData;
}

mFlixService.findMovie = async (movieId) => {
    let movieData = await mFlixDb.findMovie(movieId);
    console.log(`findMovie: ${movieData}`);

    if(movieData == null){
        throwError('Movie not available for booking now', 404);
    }

    return movieData;
}


/* Books the required number of tickets for an existing movie */
mFlixService.bookTickets = async (userId, bookingObj) => {
    let movieAvailableData = await mFlixDb.checkAvailibility(bookingObj);

    if(movieAvailableData == null){
        // Movie doesn't exist
        console.log(`Movie ${bookingObj.movieId} does not exist`);
        throwError('Bookings are not open for this movie yet!!', 404);
    }
    else if(movieAvailableData.availability == 0){
        // Movie is soldout
        console.log(`Tickets are sold out for ${bookingObj.movieId}`);
        throwError(`Housefull!! Bookings Closes`, 404);
    }
    else if(movieAvailableData.availability < bookingObj.noOfTickets || bookingObj.noOfTickets <= 0){
        // Not enough seats
        console.log(`Only ${movieAvailableData.availability} tickets available for movie ${bookingObj.movieId}`);
        throwError(`Only ${movieAvailableData.availability} seats availble`, 404);
    } else{
        // Movie can be booked
        console.log(`${userId} is booking ${bookingObj.noOfTickets} tickets for movie ${bookingObj.movieId}`);
        try{
            let bookingInfo = await mFlixDb.bookTicket(userId, bookingObj);
            console.log(bookingInfo);
            return bookingInfo;
        } catch (err){
            throwError('Booking Failed', 500);
        }
    }
}

/* Retrieves all the bookings made by an existing user */
mFlixService.previousBookings = async (userId) => {
    const userPreviousBookings = await mFlixDb.previousBookings(userId);

    console.log(`prevBookings1: ${userPreviousBookings}`);

    if(userPreviousBookings == null){
        console.log(`User ${userId} is not a registered user`);
        throwError('Please try again with a registered userId', 403);
    }
    else if(userPreviousBookings.length == 0){
        console.log('No bookings');
        throwError("You haven't booked with us yet", 404);
    }
    // console.log(`prevBookings: ${userPreviousBookings}`);
    return userPreviousBookings;
}

module.exports = mFlixService;