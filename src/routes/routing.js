const express = require('express');
const router = express.Router();
const setupdb = require('../model/setupDb');
const mFlixService = require('../service/users');
const requestLogger = require('../utilities/winstonRequest');

/* Sets up and Initializes the database */
router.get('/setupdb', async (req, res, next) => {
    try {
        let successResponse = await setupdb();
        res.json(successResponse)
    } catch (err) {
        next(err)
    }
});

/* Retrieves all the movies available for booking */
router.get('/movies', async (req, res, next) => {
    try{
        let successResponse = await mFlixService.showMovies();
        console.log('/movies: '+successResponse);
        res.json({"response": successResponse});
    } catch(err){
        next(err);
    }
})

/* Retrieves a selected movie using the movie Id */
router.get('/movie/:movieId', async (req, res, next) => {
    try{
        let successResponse = await mFlixService.findMovie(req.params.movieId);
        console.log(`/movie/${req.params.movieId}: ${successResponse}`);
        res.json({"response": successResponse});
    } catch(err){
        next(err);
    }
})

/* Retrieves all the bookings made by an existing user */
router.get('/bookings/:id', async (req, res, next) => {
    try{
        console.log(`inside bookings/id try ${req.params.id}`);
        //requestLogger.info(`inside bookings/id try ${req.params.id}`);
        let successResponse = await mFlixService.previousBookings(req.params.id);
        console.log('successResponse: '+successResponse);
        res.json({"bookings" : successResponse});
    } catch(err){
        console.log('inside bookings/id catch');
        next(err);
    }
});

/* Books the required number of tickets for an existing movie */
router.put('/book/:userId', async (req, res, next) => {
    req._routeWhitelists.body = ['movieId', 'noOfTickets'];
    try{
        console.log(`inside book/userId try ${req.params.userId} ${req.body.movieId}`);
        let successResponse = await mFlixService.bookTickets(req.params.userId, req.body);
        console.log(successResponse);
        res.json({"message": `Booking Successful!! Booking Id: ${successResponse.bookingId}, You need to Pay: ${successResponse.bookingCost}`});
    } catch(err){
        console.log('inside book/userId catch');
        next(err);
    }
});

/* send a 400 Response to the user with message Invalid Request for any other request URLs which are not mentioned here */
router.get('/*', (req, res, next) => {
    res.status(400);
    res.send('Invalid Request');
})

module.exports = router;
