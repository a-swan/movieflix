const expressWinston = require('express-winston');
const winston = require('winston');
const logFormat = require('./logFormat');
const path = require('path');

const winstonRequest = expressWinston.logger({
    transports: [
        new winston.transports.File({
            name: 'Request Log File',
            filename: path.join(__dirname, '../logs/requestLogger.log'),
            format: winston.format.combine(logFormat)
        }),
        // new winston.transports.Console({
        //     format: winston.format.json()
        // })
    ]
});

module.exports = winstonRequest;