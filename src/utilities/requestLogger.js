const fs = require('fs');

/* Logs all the requests into requestLogger.txt file */
let requestLogger = (req, res, next) => {
    let logMessage = `${new Date()} - ${req.method} - ${req.path}\n`;
    fs.appendFile('./src/logs/requestLogger.txt', logMessage, (err) => {
        if(err) next(err);
        else next();
    })
}

module.exports = requestLogger;
