const fs = require('fs');

/* Logs all the errors into errorLogger.txt file */
let errorLogger = (err, req, res, next) => {
    let errorMessage = `${new Date()} - ${err.stack}\n`;
    fs.appendFile('./src/logs/errorLogger.txt', errorMessage, (error) => {
        if (error) console.log(`logging error failed - ${error}`)
        else{
            err.status ? res.status(err.status) : res.status(500);
            res.json({message: err.message});
        }
    })
}

module.exports = errorLogger;
