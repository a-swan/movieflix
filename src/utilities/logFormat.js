const winston = require('winston');

let logFormat = winston.format.combine(
    winston.format.label({
        label:'[LOGGER]'
    }),
    winston.format.timestamp({
        format:"YY-MM-DD HH:mm:ss"
    }),
    winston.format.printf((info) => {
        let msgString = `${info.label} ${info.timestamp} ${info.level} : ${info.message}`;

        if(info.meta.req.body){
            msgString = `${msgString}: ${JSON.stringify(info.meta.req.body)}`;
        }

        if(info.meta.message){
            msgString = `${msgString}: ${info.meta.message}`;
        }
        return msgString;
    })
);

module.exports = logFormat;