const expressWinston = require('express-winston');
const winston = require('winston');
const logFormat = require('./logFormat');
const path = require('path');

const winstonError = expressWinston.errorLogger({
    transports: [
        new winston.transports.File({
            name: 'Error Log File',
            filename: path.join(__dirname, '../logs/errorLogger.log'),
            format: winston.format.combine(logFormat)
        }),
    ]
});

module.exports = winstonError;