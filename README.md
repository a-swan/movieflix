# Node JS Beyond the Basics Assignment 1

## Problem Statement

**movieFlix** is an online cinema booking portal where they show three different old blockbuster movies every week on their platform. To watch the movie, you need to buy a ticket where tickets are available on their portal and are in limited stocks. They want to automate certain process and want to create a new backend (server side) using express.

To ensure their presence in the market, they want to start with the following functionalities:

1. Users must be able to view all the movies available for booking
2. User should also be able to search for a selected movie
3. User should be able to book the required number of seats for a movie
4. User should be able to view all the bookings done

## Getting Started

1. **[Click here to download](https://lex.infosysapps.com/content-store/Infosys/Infosys_Ltd/Public/lex_auth_013109097487302656454/web-hosted/assets/movieFlixToTrainee1603008490269.zip)** the **movieFlix** express project
2. Extract the files from downloaded zip folder and open in VS code
3. Execute **npm install** command to install the required Packages in the project
4. Execute the **node app** command to start the web service
5. Go to the url http://localhost:3000/setupdb - to setup database (You will get success resposnse - **Data intialised successfully**)
6. For necessary instructions to implement the required functionalities, refer the respective files of the ToTrainee

## Expected Implementations

1. Configure the requestLogger, errorLogger and body Parser middlewares
2. Define a route **/book/:userId** (PUT request) to book the required number of tickets 
3. Define a route **/bookings/:id** (GET request) to fetch all the bookings of a user
4. Define another route to handle the invalid requests with response code 400

Refer the next page for sample Inputs and Outputs.

The below table contains the Requests and the corresponding outputs. Please note that the outputs given here are based on the same execution order as given in the table. Any change in execution order may lead to difference in outputs.

| **Request URL and Body**                                     | **Sample Output**                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| GET http://localhost:3000/movies                             | [ { "movieName": "Iron Man 2", "movieCostPerTicket": 250, "movieId": 1001, "availability": 60, "__v": 0 }, { "movieName": "Fight Club", "movieCostPerTicket": 300, "movieId": 1002, "availability": 2, "__v": 0 }, { "movieName": "Beautiful Mind", "movieCostPerTicket": 200, "movieId": 1003, "availability": 10, "__v": 0 } ] |
| GET http://localhost:3000/movie/1002                         | { "movieName": "Fight Club", "movieCostPerTicket": 300, "movieId": 1002, "availability": 2, "__v": 0 } |
| GET http://localhost:3000/movie/1100                         | { "message": "Movie Not available for Booking Now" }         |
| GET http://localhost:3000/bookings/jerry123@gmail.com        | { "message": "You havent booked with us yet" }               |
| PUT http://localhost:3000/book/jerry123@gmail.comContent-Type: application/json{ "movieId":1002, "noOfTickets":2 } | { "message": "Booking Successful!! Booking Id: 2001, You need to Pay: 600" } |
| PUT http://localhost:3000/book/jerry123@gmail.comContent-Type: application/json{ "movieId":1002, "noOfTickets":2 } | { "message": "Housefull!! Bookings Closed" }                 |
| PUT http://localhost:3000/book/jerry123@gmail.comContent-Type: application/json{ "movieId":2002, "noOfTickets":2 } | { "message": "Bookings are not open for this movie yet!!" }  |
| GET http://localhost:3000/bookings/jerry123@gmail.com        | [ { "_id": "5f8bdc61f142bf13f0876240", "movieId": 1002, "noOfTickets": 2, "movieName": "Fight Club", "bookingCost": 600, "bookingId": 2001 } ] |
| POST http://localhost:3000/about-us                          | { "message": "Invalid Request" }                             |

Next you can find some additional requirements to be implemented in movieFlix application as the next assignment.

## Problem Statement

**MovieFlix** Application owners seem to be very happy with the support and response they are getting from the users. Now they want to give some more functionalities in return to the User. They want us to implement the following requirements:

1. User should be able to update the number of seats in the booking
2. User should be able to cancel the booking done using the booking Id 

## Expected Implementations

1. Define the routes with proper route path and method for updating bookings and cancelling booking
2. Implement proper business logic and display appropriate error messages whereever required
3. In case of update and cancellation, the no of seats in movies collection should also be updated
4. If the user cancels a booking, it should be removed from the users booking array 

**Note:** These requirements are to be implemented from scratch in the same project as given in Assingment 1 and hence no starter code is given for the same.